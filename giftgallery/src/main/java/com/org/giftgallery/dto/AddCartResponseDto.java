package com.org.giftgallery.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddCartResponseDto {
	private String message;
	private int statuscode;
	private long cartId;
}
