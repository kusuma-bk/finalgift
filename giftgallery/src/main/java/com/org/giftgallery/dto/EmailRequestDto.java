package com.org.giftgallery.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmailRequestDto {

	private List<RecipientRequestListDto> listOfRecipientRequestListDto;
	private String senderEmailId;
	private String senderPhoneNumber;
	private String senderName;

}
