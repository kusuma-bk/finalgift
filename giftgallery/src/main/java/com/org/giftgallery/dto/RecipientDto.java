package com.org.giftgallery.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RecipientDto {
	private String recipientName;
	private String recipientEmail;
	private String recipientPhoneNumber;
	//private long cartId;
	private String message;

}
