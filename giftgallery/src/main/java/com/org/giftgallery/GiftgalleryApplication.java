package com.org.giftgallery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiftgalleryApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiftgalleryApplication.class, args);
	}

}
