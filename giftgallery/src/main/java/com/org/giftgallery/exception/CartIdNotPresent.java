package com.org.giftgallery.exception;

public class CartIdNotPresent extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public CartIdNotPresent(String message) {
		super(message);
	}

}
