package com.org.giftgallery.exception;

public class CartException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public CartException(String message) {
		super(message);
	}
}
