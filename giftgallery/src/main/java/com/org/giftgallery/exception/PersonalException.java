package com.org.giftgallery.exception;

public class PersonalException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public PersonalException(String message) {
		super(message);
	}

}
