package com.org.giftgallery.exception;

public class OrderListNotPresent extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public OrderListNotPresent(String message) {
		super(message);
	}

}
