package com.org.giftgallery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.giftgallery.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserId(long userId);

}
