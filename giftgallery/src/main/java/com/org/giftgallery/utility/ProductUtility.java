package com.org.giftgallery.utility;

public class ProductUtility {
	public static final String PRODUCTS_LIST_ERROR = "There are no products";
	public static final int PRODUCTS_LIST_ERROR_STATUS = 606;
	public static final int PRODUCTS_LIST__STATUS = 607;
	public static final String QUANTITY_ERROR = "There are no products";
	public static final int QUANTITY_ERROR_STATUS = 608;
	public static final String CART_ERROR = "There are no products";
	public static final int CART_ERROR_STATUS = 609;
	public static final String CART_MESSAGE = "Your products are added successfully";
	public static final int CART_MESSAGE_STATUS = 700;
	public static final String USER_EXIST_ERROR = "User is not available for this particular userId";
	public static final int USER_EXIST_ERROR_STATUS = 701;

	public static final String USER_TYPE_PERSONAL_ERROR = "You should not give more than three users";
	public static final int USER_TYPE_PERSONAL_STATUS = 702;

	public static final String USER_TYPE_CORPORATE_ERROR = "You should give atleast 5 users";
	public static final int USER_TYPE_CORPORATE_STATUS = 703;
}
